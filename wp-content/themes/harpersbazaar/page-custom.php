<?php
/*
Template Name: Runway overview 
*/
?>

<?php get_header(); ?>

			<div id="content">

				<div class="container">
			
						<div id="main" class="clearfix" role="main">
							<div class="article-pre">
								<?php if ( function_exists('yoast_breadcrumb') ) {
									yoast_breadcrumb('<p class="breadcrumbs">','</p>');
								} ?>
							</div>
							<div class="page-wrapper">
							<?php if( have_rows('runway_season', 'options') ): ?>
 							 	<div class="article-wrapper">
 							 		<h1 class="category-title barred-heading">
 							 			<span>Các mùa thời trang</span>
 							 		</h1>
							    <?php while( have_rows('runway_season', 'options') ): the_row(); ?>
							 
							 		<section class="runway_tag_link">
							        <?php $tag = get_sub_field('runway_tag');
							        // print_r($tag);
							        	echo '<a href="'.site_url('/tag/').$tag->slug.'"><h2 class="category-title">'.$tag->name.'</h2></a>';							        
							        	echo '<div class="image_wrapper">';
							        	echo '<a href="'.site_url('/tag/').$tag->slug.'"><img src='.get_sub_field("runway_image").' alt="'.$tag->name.'" title="'.$tag->name.'" /></a>';
							        	echo '</div>';
							        ?>
							        </section>
							    <?php endwhile; ?>
							    <div class="newsletter-form newsletter-form-container container">
									<div class="row">
										<h1><?php _e("Tìm Mùa Thời Trang Khác", "harpersbazaar"); ?></h1>
										<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
											<input type="hidden" value="17" name="cat" id="scat">
											<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Thu Đong 2014 RTW, Pre-fall 2013', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" /><input type="submit" class="search-submit" value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
										</form>
									</div>
								</div>
							 </div>
							 
							<?php endif; ?>
							
							<?php get_sidebar(); ?>
						</div>

					</div> <!-- end #main -->


				</div> <!-- end #inner-content -->

			</div> <!-- end #content -->

<?php get_footer(); ?>
