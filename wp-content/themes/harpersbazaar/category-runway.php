<?php get_header(); ?>

<div id="content">
	<div class="container">
		<div id="main" class="clearfix" role="main">

			<div class="article-pre">
				<?php if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p class="breadcrumbs">','</p>');
				} ?>
			</div>

			<div class="page-wrapper">

				<div class="category-content-wrapper">
	
					<?php //get_template_part("parts/trends-latest"); ?>

					<?php 
						$terms = get_terms( array(
						    'taxonomy' => 'collection',
						    'hide_empty' => false,
						    'order'	=> 'DESC',
							'orderby' => 'title',
							'parent' => 0,
						) );
					?>
					<ol class="snap_nav">
						<li class="snap_selected"><?php _e('All','harpersbazaar'); ?></li>
						<?php foreach ($terms as $key => $term): ?>
							<li><a href="<?php echo get_term_link( $term->term_id, 'collection' ); ?>"><?php echo $term->name; ?></a></li>
						<?php endforeach ?>
					</ol>

					<div class="row">
						<?php if (have_posts()) : ?>
							<?php
								$acf_term_id = $term->term_id;
								$collections = get_terms( array(
								    'taxonomy' => 'collection',
						    		'hide_empty' => false,
								    'order'	=> 'ASC',
									'orderby' => 'title',
									'childless' => true,
									) 
								); ?>
								<?php if ( $collections ){ ?>
									<?php foreach ($collections as $key => $collection_child): ?>
										<?php 
										$args = array(
											//Type & Status Parameters
											'posts_per_page' => 3,
											'post_type'   => 'post',
											'post_status' => 'publish',
											//Order & Orderby Parameters
											'order'               => 'DESC',
											'orderby'             => 'date',
											//Tag Parameters
											'tax_query' => array(
												'relation' => 'AND',
												array(
													'taxonomy' => 'collection',
													'field'    => 'term_id',
													'terms'    => array( $collection_child->term_id ),
													// 'operator' => 'NOT IN',
												),
											),
										);
										
										$posts_collection = new WP_Query( $args ); 

										if ($posts_collection->have_posts()) { ?>

											<a href="<?php echo get_term_link($collection_child->term_id, 'collection'); ?>"><h1 class="category-title barred-heading"><span><?php echo apply_filters( 'the_title', $collection_child->name ); ?></span></h1></a>

											<?php while ($posts_collection->have_posts()) : $posts_collection->the_post(); ?>

												<?php get_template_part('parts/tax-article-content'); ?>

											<?php endwhile; ?>
										<?php } ?>
										
									<?php endforeach ?>
								<?php } ?>

								<?php //get_template_part('parts/tax-cat-year'); ?>

						<?php else : ?>

							<div id="post-not-found" class="hentry clearfix">
								<header class="article-header">
									<h1><?php _e("Chưa có bài viết trong chuyên mục này", "harpersbazaar"); ?></h1>
								</header>
							</div>

						<?php endif; ?>
					</div> <!-- .row -->

				</div> <!-- .category-content-wrapper -->

				<?php get_sidebar(); ?>

			</div> <!-- end .page-wrapper -->

		</div> <!-- end #main -->
	</div> <!-- end .container -->

</div> <!-- end #content -->

<section class="tags moremore">
	<h2 class="more-heading"><span><?php _e("Xem thêm...", "harpersbazaar"); ?></span></h2>
	<ul class=more_tags><?php 
		$curr_cat = get_cat_id( single_cat_title("",false) );
		top_tags($curr_cat); 
		?></ul>
</section>

<?php get_footer(); ?>
