<?php get_header(); ?>

<div id="content">
	<div class="container">
		<div id="main" class="clearfix" role="main">

			<div class="article-pre">
				<?php if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p class="breadcrumbs">','</p>');
				} ?>
			</div>

			<div class="page-wrapper">

				<div class="category-content-wrapper alpha-category-content-wrapper">
	
					<?php // get_template_part("parts/trends-latest"); ?>

					<h1 class="category-title barred-heading">
						<span><?php single_cat_title(); ?></span>
					</h1>

					<?php 
					// Alphabet nav
					if (function_exists('wp_snap')) { echo wp_snap('firstload=all'); }
					?>

					<?php if (have_posts()) : ?>

					<?php 
						if ( isset($_GET['snap']) ) {
							if ($_GET['snap'] == 'misc') {
								$snap_request = 'misc';
								get_template_part('parts/cat-people-misc-all');
							} else {
								$snap_request = urldecode($_GET['snap']);
								get_template_part('parts/cat-people-content-snap');
							}
						}else{
							get_template_part('parts/cat-people-content');
						}
					?>	
					<?php else : ?>

						<div id="post-not-found" class="hentry clearfix">
							<p><?php _e("No articles in this category.", "harpersbazaar"); ?></p>
						</div>

					<?php endif; ?>
					</div> <!-- .alpha-category-content -->
					</div> <!-- .row -->

				</div> <!-- .category-content-wrapper -->

				<?php get_sidebar(); ?>

			</div> <!-- end .page-wrapper -->

		</div> <!-- end #main -->
	</div> <!-- end .container -->

</div> <!-- end #content -->

<section class="tags moremore">
	<h2 class="more-heading"><span><?php _e("Xem thêm...", "harpersbazaar"); ?></span></h2>
	<ul class=more_tags><?php 
		$curr_cat = get_cat_id( single_cat_title("",false) );
		top_tags($curr_cat); 
		?></ul>
</section>

<?php get_footer(); ?>
