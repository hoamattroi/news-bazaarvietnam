<div id="article-slideshow" class="carousel slide article-slideshow" data-ride="carousel" data-interval="false">
	<!-- Wrapper for slides -->
	<div class="carousel-inner">

		<?php 
		$slideCounter = 1;
		while(has_sub_field('slideshow')):
			if ($slideCounter == 1) {
				$activeClass = "active";
			} else {
				$activeClass= "";
			}
		?>
			<?php $slideLayout = get_sub_field('slide_layout'); ?>
			<div class="item <?php echo $slideLayout; ?> <?php echo $activeClass; ?>">
				<figure class="slideshow-image">
					<?php
						$slideImage = get_sub_field('slide_image');
						$slideImageFullURL = $slideImage['sizes']['large'];
						if ($slideLayout == "landscape") {
							$slideImageURL = $slideImage['sizes']['slideshow-landscape'];
						} else {
							$slideImageURL = $slideImage['sizes']['slideshow-portrait'];
						}	
					?>
					<img src="<?php echo $slideImageURL; ?>" alt="...">
					<figcaption class="slideshow-image-actions">
						<a class="action-pinterest" href="pinterest">P</a>
						<a class="action-expand" data-toggle="modal" data-target="#expand-<?php echo $slideCounter; ?>">+</a>
					</figcaption>
				</figure>
				<div class="carousel-caption">
					<h2>Bộ sưu tập thu đông 2014</h2>
					<p>Kate Wendelborn models The Line’s new demim collaboration with 3x1 at the site’s brick and mortar counterpart</p>
				</div>
				<!-- Modal -->
				<div class="modal fade" id="expand-<?php echo $slideCounter; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-body">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<img src="<?php echo $slideImageFullURL; ?>"></img>
							</div>
						</div>
					</div>
				</div>
				<!-- /Modal -->
			</div>
		<?php 
		$slideCounter++;
		endwhile;
		?>
	</div>

	<!-- Controls -->
	<a class="left carousel-control" href="#article-slideshow" data-slide="prev">
		Previous
	</a>
	<a class="right carousel-control" href="#article-slideshow" data-slide="next">
		Next
	</a>
</div>