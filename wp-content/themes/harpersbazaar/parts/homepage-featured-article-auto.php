
<article class="featured-primary">
	<figure>
		<div class="featured-primary-imgwrap">
			<a href="<?php the_permalink(); ?>">
				<?php
					if ( has_post_thumbnail() ) {
						the_post_thumbnail('square-720');
					}
				?>
			</a>
			<a class="pinterest" href="http://www.pinterest.com/pin/create/button/?url=<?php the_permalink() ?>&amp;media=<?php echo $featuredArticleFullImage; ?>&amp;description=<?php the_title(); ?>">Pinterest</a>
		</div>
		<figcaption>
			<h2 class="h2">
				<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
			</h2>
			<p class="byline vcard">
				<?php
				printf(__('%1$s.', 'harpersbazaar'), get_the_category_list(', '));
				?>
			</p>
		</figcaption>	
	</figure>
</article>
