<?php 
$postCount = 0;
// $postsPerRow = 0;0
$prevTitleInitial = "";
$snap_char = 'misc';
if (have_posts()) : while (have_posts()) : the_post();
	$postCount++;
	// $postsPerRow++;
	$title = get_the_title();
	// $title = iconv('UTF-8', 'ASCII//TRANSLIT', $title);
	// Get first character of title
	$currTitleInitial = mb_substr($title, 0, 1, 'utf8');
	$currTitleInitial = stripVN($currTitleInitial);
	if ($currTitleInitial !== $prevTitleInitial) {
		$snap_char = (ctype_alpha($currTitleInitial)) ? $currTitleInitial : 'misc';
		// $postsPerRow = 1;
		if ( $postCount != 1 ) {
			echo "</div> <!-- .alpha-category-content -->";
			echo "</div> <!-- .row -->";
		}
		echo "<div class=\"row\">";
		echo "<a href=\"".get_term_link( get_queried_object()->term_id, 'category' )."?snap=$snap_char\">";
		echo "<span class=\"alpha-letter\">$currTitleInitial</span>";
		echo "</a>";
		echo "<div class=\"alpha-category-articles\">";
	}
	$prevTitleInitial = $currTitleInitial;
	// if ( $postsPerRow > 6 ) continue;
	?>

		<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
			<header class="article-header">
				<?php the_post_thumbnail('square-360'); ?>
				<h2 class="h2"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
				<p>
			</header> <!-- end article header -->
		</article> <!-- end article -->

	<?php endwhile; ?>
<?php endif; ?>