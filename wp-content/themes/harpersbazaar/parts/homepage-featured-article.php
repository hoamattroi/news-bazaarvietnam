
	<?php
	$featuredArticle = get_field('featured_article', 'option');
	
	$featuredImage = get_field('featured_article_image', 'option');
	$featuredHeading = get_field('featured_article_heading', 'option');
	if( $featuredArticle ): ?>
		<?php foreach( $featuredArticle as $post): // variable must be called $post (IMPORTANT) ?>
			<?php 
			setup_postdata($featuredArticle);
			//$featuredArticleID = get_the_id();
			$featuredArticleFullImage = get_post_thumbnail_id();
			$featuredArticleFullImage = wp_get_attachment_thumb_url($featuredArticleFullImage);
			?>
			<article class="featured-primary">
				<figure>
					<div class="featured-primary-imgwrap">
						<a href="<?php the_permalink(); ?>">
							<?php
							if ($featuredImage) {
								$featuredImage = getImgServerUrl($featuredImage["sizes"]["square-720"]);
								
								echo "<img src=\"$featuredImage\" />";

							} else {
								the_post_thumbnail('square-720');
							}
							?>
						</a>
						<a class="pinterest" href="http://www.pinterest.com/pin/create/button/?url=<?php the_permalink() ?>&amp;media=<?php echo $featuredArticleFullImage; ?>&amp;description=<?php the_title(); ?>">Pinterest</a>
						
					</div>
					<figcaption>
						<h2 class="h2">
							<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
								<?php
								if ($featuredHeading) {
									echo $featuredHeading;
								} else {
									the_title();
								}
								?>
							</a>
						</h2>
						<p class="byline vcard">
							<?php
							printf(__('%1$s.', 'harpersbazaar'), get_the_category_list(', '));
							?>
						</p>
					</figcaption>	
				</figure>
			</article>
		<?php endforeach; ?>
		<?php wp_reset_postdata(); ?>
	<?php endif; ?>

	<?php //get_template_part( "parts/people"); ?>
