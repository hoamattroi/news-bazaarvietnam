<?php
$tabCounter = 1;
$tabsHTML = "";
// if( have_rows('brand_information') ) {
// 	while ( have_rows('brand_information') ) : the_row();
// 		if ($tabCounter == 1) {
// 			$tabActiveClass = "active";
// 		} else {
// 			$tabActiveClass= "";
// 		}
// 		$tabHeading = get_sub_field("tab_title");
// 		$tabsHTML.="<li class=\"$tabActiveClass brand_tabs\" style=\"width:19%;text-align:center;\" ><a href=\"#tab-$tabCounter\" data-toggle=\"pill\">$tabHeading</a></li>";
// 		$tabCounter++;
// 	endwhile;
// }

// if ($tabCounter >2) {
	// echo "<nav class=\"nav-tabbed\">";
	// 	echo "<ul class=\"nav nav-pills\">";
	// 		echo $tabsHTML;
	// 	echo "</ul>";
	// echo "</nav>";
// }
?>

<?php
if( have_rows('brand_information') ):
?>
<div class="tab-content">
<?php
	$tabCounter = 1;
	
	while ( have_rows('brand_information') ) : the_row();
		if ($tabCounter == 1) {
			$tabActiveClass = "active";
		} else {
			$tabActiveClass= "";
		} ?>

		<div class="<?php echo $tabActiveClass; ?> brand_page_tab" id="tab-<?php echo $tabCounter; ?>">
			
			<?php if(get_sub_field('tab_gallery')) : ?>
			<div id="article-slideshow-<?php echo $tabCounter; ?>" class="brand-page-gallery article-slideshow article-slideshow-gallery carousel slide" data-ride="carousel" data-interval="false">
				<!-- Wrapper for slides -->
				<div class="carousel-inner">
					<?php 
					
						$slideCounter = 0;
						$gallery = get_sub_field("tab_gallery");
	
						foreach($gallery as $key => $slideImage):
							$slideCounter++;
							if ($slideCounter == 1) {
								$activeClass = "active";
							} else {
								$activeClass= "";
							}
						
						?>
							<div class="item <?php echo $activeClass; ?>">
								<figure class="slideshow-image" <?php if($slideCounter==0){ echo 'itemprop="thumbnailUrl"'; } ?>>
									<?php
										$slideImageLargeURL = $slideImage['sizes']['large'];
										$slideImageURL = $slideImage['sizes']['brandlarge'];
										
										$slideImageURL = getImgServerUrl ($slideImageURL);
										$slideImageLargeURL = getImgServerUrl ($slideImageLargeURL);
									?>
									<a class="action-expand" data-toggle="modal" data-target="#expand-<?php echo $tabCounter; ?>-<?php echo $slideCounter; ?>"><img src="<?php echo $slideImageURL; ?>" alt="slideimg"></a>
								</figure>
								<!-- Modal -->
								<div class="modal fade" id="expand-<?php echo $tabCounter; ?>-<?php echo $slideCounter; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-body">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
												<img src="<?php echo $slideImageLargeURL; ?>"></img>
											</div>
										</div>
									</div>
								</div>
								<!-- /Modal -->
							</div>
						<?php 
						
						endforeach;
					?>
				</div><!-- .carousel-inner -->
				
				<?php if ($slideCounter > 1): ?>
				<!-- Controls -->
				<a class="left carousel-control" href="#article-slideshow-<?php echo $tabCounter; ?>" data-slide="prev">Previous</a>
				<a class="right carousel-control" href="#article-slideshow-<?php echo $tabCounter; ?>" data-slide="next">Next</a>
				<?php endif; 
			
			?>

			</div> <!-- .article-slideshow -->

			
			<?php else : ?>
			<div class="slideshow-gallery-text">
				<?php echo bz_the_content_filter(get_sub_field("tab_content")); ?>
			</div> <!-- .slideshow-gallery-text -->
			
			<?php endif; ?>
		</div> <!-- .tab-pane -->

<?php
		
	$tabCounter++; 
	endwhile; 
?>
	<div class="brand_page_tab" id="tab-<?php echo $tabCounter; ?>">
		<div class="slideshow-gallery-text">
			<?php the_content(); ?>
		</div>
	</div>

</div> <!-- .tab-content -->
<?php endif; ?>