<?php
$tabCounter = 1;
$tabsHTML = "";
if( have_rows('tabbed-slideshow') ) {
	while ( have_rows('tabbed-slideshow') ) : the_row();
		if ($tabCounter == 1) {
			$tabActiveClass = "active";
		} else {
			$tabActiveClass= "";
		}
		$tabHeading = get_sub_field("tab_heading");
		$tabsHTML.="<li class=\"$tabActiveClass\"><a href=\"#tab-$tabCounter\" data-toggle=\"pill\">$tabHeading</a></li>";
		$tabCounter++;
	endwhile;
}

if ($tabCounter >2) {
	echo "<nav class=\"nav-tabbed\">";
		echo "<ul class=\"nav nav-pills\">";
			echo $tabsHTML;
		echo "</ul>";
	echo "</nav>";
}
?>

<?php
if( have_rows('tabbed-slideshow') ):
?>
<div class="tab-content">
<?php
	$tabCounter = 1;
	
	while ( have_rows('tabbed-slideshow') ) : the_row();
		if ($tabCounter == 1) {
			$tabActiveClass = "active";
		} else {
			$tabActiveClass= "";
		}
		if( get_row_layout() == 'slideshow-tab' ):
?>
			<div class="tab-pane <?php echo $tabActiveClass; ?>" id="tab-<?php echo $tabCounter; ?>">

				<div id="article-slideshow-<?php echo $tabCounter; ?>" class="article-slideshow carousel slide" data-ride="carousel" data-interval="false">
					<!-- Wrapper for slides -->
					<div class="carousel-inner">
						<?php 
						$slideCounter = 1;
						$totalSlides = count( get_sub_field('tab_slides') );
						while(has_sub_field('tab_slides')):

							if ($slideCounter == 1) {
								$activeClass = "active";
								
							} else {
								$activeClass= "";
							}
						?>
							<?php $slideLayout = get_sub_field('slide_layout'); ?>
							<div class="item <?php echo $slideLayout; ?> <?php echo $activeClass; ?>">
								<figure class="slideshow-image" itemprop="thumbnailUrl">
									<?php
										$slideImage = get_sub_field('slide_image');
										$slideImageLargeURL = $slideImage['sizes']['large'];
										if ($slideLayout == "landscape") {
											$slideImageURL = $slideImage['sizes']['slideshow-landscape'];
										} else {
											$slideImageURL = $slideImage['sizes']['slideshow-portrait'];
										}
										$slideImageURL = getImgServerUrl ($slideImageURL);
										$slideImageLargeURL = getImgServerUrl ($slideImageLargeURL);
									?>
									<a class="action-expand" data-toggle="modal" data-target="#expand-<?php echo $tabCounter; ?>-<?php echo $slideCounter; ?>"><img src="<?php echo $slideImageURL; ?>" alt="slideimg"></a>
									<figcaption class="slideshow-image-actions">
										<a class="action-pinterest" href="http://www.pinterest.com/pin/create/button/?media=<?php echo $slideImageLargeURL; ?>&amp;description=<?php the_title(); ?>&amp;url=<?php the_permalink() ?>#slide-<?php echo $slideCounter;?>">P</a>
										<a class="action-expand" data-toggle="modal" data-target="#expand-<?php echo $tabCounter; ?>-<?php echo $slideCounter; ?>">+</a>
										<span class="slideshow-counter"><?php echo $slideCounter; ?> <?php _e( " / ", 'harpers-bazaar' ); ?> <?php echo $totalSlides; ?></span>
									</figcaption>
								</figure>
								<div class="carousel-caption">
									<?php echo bz_the_content_filter(get_sub_field("slide_text")); ?>
								</div>
								<!-- Modal -->
								<div class="modal fade" id="expand-<?php echo $tabCounter; ?>-<?php echo $slideCounter; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-body">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
												<img src="<?php echo $slideImageLargeURL; ?>"></img>
											</div>
										</div>
									</div>
								</div>
								<!-- /Modal -->
							</div>
						<?php 
						$slideCounter++;
						endwhile;
						?>
					</div>

					<?php if ($slideCounter > 2): ?>
					<!-- Controls -->
					<a class="left carousel-control" href="#article-slideshow-<?php echo $tabCounter; ?>" data-slide="prev">Previous</a>
					<a class="right carousel-control" href="#article-slideshow-<?php echo $tabCounter; ?>" data-slide="next">Next</a>
					<?php endif; ?>

				</div> <!-- .article-slideshow -->
			</div> <!-- .tab-pane -->

<?php 
		endif;
		if( get_row_layout() == 'gallery-tab' ):
?>
			<div class="tab-pane <?php echo $tabActiveClass; ?>" id="tab-<?php echo $tabCounter; ?>">

				<div id="article-slideshow-<?php echo $tabCounter; ?>" class="article-slideshow article-slideshow-gallery carousel slide" data-ride="carousel" data-interval="false">
					<!-- Wrapper for slides -->
					<div class="carousel-inner">
						<?php 
						$slideCounter = 0;
						$gallery = get_sub_field("tab_gallery");
						$totalSlides = sizeOf( $gallery );
						foreach($gallery as $key => $slideImage):
							$slideCounter++;
							if ($slideCounter == 1) {
								$activeClass = "active";
							} else {
								$activeClass= "";
							}
						?>
							<div class="item <?php echo $activeClass; ?>">
								<figure class="slideshow-image" itemprop="thumbnailUrl">
									<?php
										$slideImageLargeURL = $slideImage['sizes']['large'];
										$slideImageURL = $slideImage['sizes']['slideshow-portrait'];
										
										$slideImageLargeURL = getImgServerUrl ($slideImageLargeURL);
										$slideImageURL = getImgServerUrl ($slideImageURL);
									?>
									<a class="action-expand" data-toggle="modal" data-target="#expand-<?php echo $tabCounter; ?>-<?php echo $slideCounter; ?>"><img src="<?php echo $slideImageURL; ?>" alt="slideimg"></a>
									<span class="slideshow-counter"><?php echo $slideCounter; ?> <?php _e( " / ", 'harpers-bazaar' ); ?> <?php echo $totalSlides; ?></span>
								</figure>
								<!-- Modal -->
								<div class="modal fade" id="expand-<?php echo $tabCounter; ?>-<?php echo $slideCounter; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-body">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
												<img src="<?php echo $slideImageLargeURL; ?>"></img>
											</div>
										</div>
									</div>
								</div>
								<!-- /Modal -->
							</div>
						<?php 
						
						endforeach;
						?>
					</div>

					<?php if ($slideCounter > 1): ?>
					<!-- Controls -->
					<a class="left carousel-control" href="#article-slideshow-<?php echo $tabCounter; ?>" data-slide="prev">Previous</a>
					<a class="right carousel-control" href="#article-slideshow-<?php echo $tabCounter; ?>" data-slide="next">Next</a>
					<?php endif; ?>

				</div> <!-- .article-slideshow -->
				<div class="slideshow-gallery-text">
				
					<?php echo bz_the_content_filter(get_sub_field("tab_text")); ?>
				</div>

				<!-- Indicators -->
				<div id="gallery-indicator-carousel-<?php echo $tabCounter; ?>" class="gallery-indicators carousel slide" data-ride="carousel" data-interval="false">
					<div class="carousel-inner">
						<div class="item active">
						<?php 
						$slideCounter = 0;
						reset($gallery);
						foreach($gallery as $key => $slideImage):
							$slideCounter++;
							if ($key == 0) {
								$activeClass = "active";
							} else {
								$activeClass = "";
							}
							$slideImageURL = getImgServerUrl ($slideImage['sizes']['slideshow-portrait']);
						?>
							<div data-target="#article-slideshow-<?php echo $tabCounter; ?>" data-slide-to="<?php echo $key; ?>" class="<?php echo $activeClass; ?> gallery-indicator-link">
								<img src="<?php echo $slideImageURL; ?>" alt="<?php echo $slideCounter; ?>" />
							</div>
						<?php
							if (($slideCounter%4) == 0) :
						?>
								</div>
								<div class="item">
						<?php
							endif;
						endforeach;
						?>
						</div> <!-- .item -->
					</div> <!-- .carousel-inner -->

					<?php if ($slideCounter > 1): ?>
					<!-- Controls -->
					<a class="left carousel-control" href="#gallery-indicator-carousel-<?php echo $tabCounter; ?>" data-slide="prev">Previous</a>
					<a class="right carousel-control" href="#gallery-indicator-carousel-<?php echo $tabCounter; ?>" data-slide="next">Next</a>
					<?php endif; ?>

				</div> <!-- .gallery-indicators -->
			</div> <!-- .tab-pane -->

<?php
		endif;
	$tabCounter++; 
	endwhile; 
?>

</div> <!-- .tab-content -->
<?php endif; ?>