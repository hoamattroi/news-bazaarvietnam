<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">

	<header class="article-header">

		<a href="<?php the_permalink() ?>"><?php the_post_thumbnail('square-360'); ?></a>

		<h2 class="h2"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
		<p class="byline vcard">
			<?php printf(__('trong %1$s.', 'harpersbazaar'), get_the_category_list(', ')); ?>
		</p>

	</header> <!-- end article header -->

	<footer class="article-footer">

	</footer> <!-- end article footer -->

</article> <!-- end article -->