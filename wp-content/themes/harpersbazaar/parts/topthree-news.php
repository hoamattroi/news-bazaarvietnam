<section class="top-three">
	<div class="container clearfix">
		<div class="row">
			<?php if ( is_front_page() ){ ?>
				<h1 class="barred-heading"><span><?php _e("Hottest - Latest - Newest", 'harpersbazaar'); ?></span></h1>
			<?php }else{ ?>
				<h2 class="barred-heading"><span><?php _e("Hottest - Latest - Newest", 'harpersbazaar'); ?></span></h2>
			<?php } ?>

			<?php
			$args = array(
				// 'author'      => '1,2,3,',
				// 'cat'              => 1,
				// 'category__in'     => array(1, 2),
				'post_status' => 'publish',
				// 'post_type' => 'any',
				'post_type' => array(
					'post',
					'thuong-hieu',
					'chan-dung',
					'runway',
					'fashion-trends',
					// 'custom_type'
					),
				'order'               => 'DESC',
				'orderby'             => 'date',
				'ignore_sticky_posts' => true,
				'posts_per_page'	=> 3,
				'showposts' 		=> 3,
				// 'posts_per_archive_page' => 12,
				// 'offset'                 => 3,
			);
			
			if ( is_category() ) {
				$term_id = get_queried_object()->term_id;
				$m_args = array('cat' => $term_id,);
				$args = array_merge($args, $m_args);
			}elseif ( is_tag() ) {
				$term_id = get_queried_object()->term_id;
				$m_args = array('tag_id' => $term_id);
				$args = array_merge($args, $m_args);
			}elseif ( is_author() ){
				$m_id = get_queried_object()->ID;
				$m_args = array('author' => $m_id);
				$args = array_merge($args, $m_args);
			}
		
			$query = new WP_Query( $args );
			if ( $query->have_posts() ) {
				$counter = 1;
				while ( $query->have_posts() ) : $query->the_post(); 
				$articleCategory = get_the_category();
				$articleCategoryName = $articleCategory[0]->name;
				$articleCategoryID = $articleCategory[0]->cat_ID;
				$articleCategoryLink = get_category_link($articleCategoryID);
				$articleThumbnail = wp_get_attachment_image_url( get_post_thumbnail_id(get_the_id()), 'square-360' );
				$articleThumbnail = link_to_img_server($articleThumbnail);
				?>
				<article itemscope itemtype="http://schema.org/NewsArticle">
					<meta itemprop="author" content="<?php echo get_the_author(); ?>"/>
					<div itemprop="publisher" itemscope itemtype="http://schema.org/Organization">
				        <meta itemprop="name" content="Sunflower Media" />
				        <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
				        	<meta itemprop="url" content="http://sunflower.vn/wp-content/themes/sunflower/assets/img/sunflower-market-logo-trans.png">
				            <meta itemprop="width" content="171">
				            <meta itemprop="height" content="52">
				        </div>
				    </div>
				    <div itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
			            <meta itemprop="url" content="<?php echo $articleThumbnail; ?>">
			            <meta itemprop="width" content="360">
			            <meta itemprop="height" content="360">
		            </div>
					<figure class="row">
						<div class="thumb-wrap">
							<div class="top-three-thumb" itemprop="thumbnailURL">
								<a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail(get_the_id(),'square-360'); ?></a>
							</div>
						</div>
						<span class="top-three-number"><?php echo $counter; ?></span>
						<figcaption>
							<a href="<?php the_permalink(); ?>" class="imgTitle" itemprop="headline"><?php the_title(); ?></a>
							<div class="date"><time pubdate itemprop="datePublished"><?php echo get_the_date("d-m-Y"); ?></time> trong <a href="<?php echo $articleCategoryLink; ?>"><?php echo $articleCategoryName; ?></a></div>
						</figcaption>
					</figure>
				</article>
				<?php
				$counter++;
				endwhile; 
				wp_reset_query();
			}
			?>

		</div>
	</div>
</section>