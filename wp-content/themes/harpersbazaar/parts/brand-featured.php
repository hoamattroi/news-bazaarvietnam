<?php global $featured_brand_ad; ?>
<aside class="brand-featured">
	<div class="brand-featured-logo">
		<?php echo $featured_brand_ad; ?>
	</div>
	<div id="brand-featured-carousel" class="carousel slide" data-ride="carousel">
		<!-- Wrapper for slides -->
		<div class="carousel-inner">
			<?php 
			$brandCatID = "";
			$postArgs = array(
				'orderby' => 'post_date',
				'order' => 'DESC',
				'post_status' => 'publish',
				'post_type' => 'thuong-hieu',  
				'posts_per_page' => 3,
			);
			$posts = get_posts($postArgs);
			
			$activeClass = "active";
			foreach( $posts as $post ) :
				setup_postdata($post);
			?>
				<a href="<?php the_permalink(); ?>" class="item <?php echo $activeClass; ?>">
					<figure>
						<?php the_post_thumbnail('square-360'); ?>
						<figcaption><span><span><?php the_title(); ?></span></span></figcaption>
					</figure>
				</a>
			<?php
			$activeClass = "";
			endforeach;
			wp_reset_postdata();
			?>
		</div>
		<!-- Controls -->
		<a class="left carousel-control" href="#brand-featured-carousel" data-slide="prev">Previous</a>
		<a class="right carousel-control" href="#brand-featured-carousel" data-slide="next">Next</a>
	</div>
</aside>