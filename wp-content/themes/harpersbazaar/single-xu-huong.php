<?php get_header(); ?>

<div id="content">
	<div class="container">
		<div id="main" class="clearfix" role="main">

			<div class="article-pre">
				<?php if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p class="breadcrumbs">','</p>');
				} ?>
			</div>

			<div class="page-wrapper">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<div class="article-wrapper">
				
					<article class="article-primary article-trend" id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/NewsArticle">
						<?php 
							$articleThumbnail = wp_get_attachment_image_url( get_post_thumbnail_id(get_the_id()), 'square-360' );
							$articleThumbnail = link_to_img_server($articleThumbnail);
						?>
						<div itemprop="publisher" itemscope itemtype="http://schema.org/Organization">
					        <meta itemprop="name" content="Sunflower Media" />
					        <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
					        	<meta itemprop="url" content="http://sunflower.vn/wp-content/themes/sunflower/assets/img/sunflower-market-logo-trans.png">
					            <meta itemprop="width" content="171">
					            <meta itemprop="height" content="52">
					        </div>
					    </div>
					    <div itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
				            <meta itemprop="url" content="<?php echo $articleThumbnail; ?>">
				            <meta itemprop="width" content="360">
				            <meta itemprop="height" content="360">
			            </div>

						<header class="article-header">
							<h1 class="barred-heading">
								<span itemprop="headline"><?php _e("Xu Hướng: ", "harpersbazaar"); ?><?php the_title(); ?></span>
							</h1>
							<?php
								$myExcerpt = get_the_excerpt();
								$tags = array("<p>", "</p>");
								$myExcerpt = str_replace($tags, "", $myExcerpt);
  
							?>
							<p><?php echo $myExcerpt; ?></p>

						</header> <!-- end article header -->

						<section class="entry-content clearfix" itemprop="articleBody">

							<?php 
								if(get_field('tabbed-slideshow')) {
									get_template_part("parts/slideshow-tabbed");
								} else {
									the_post_thumbnail( 'full');
								}
							?>

							<?php the_content(); ?>

						</section> <!-- end article section -->

					</article> <!-- end article -->

					
					<div class="recent_post_wrap">
					<section class="recent-posts">
					
					<?php
					$curr_post_id = get_the_id();
					$curr_category = get_the_category();
					$args=array(
						'cat' => $curr_category[0]->ID,
						'post_type' => 'post',
						'post_status' => 'publish',
						'posts_per_page' => 9,
						'caller_get_posts'=> 1
					);
					$my_query = null;
					$my_query = new WP_Query($args);
					if( $my_query->have_posts() ) {
						echo '<h2 class="barred-heading"><span>'. __('BÀI VIẾT LIÊN QUAN', 'harpersbazaar').'</span></h2>';
						while ($my_query->have_posts()) : $my_query->the_post(); ?>
							<article>
								<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
									<div><?php echo get_the_post_thumbnail(get_the_ID(), 'square-360'); ?></div>
									<?php the_title(); ?>
								</a>
							</article>
				<?php	endwhile;
					 }
					wp_reset_query();  // Restore global post data stomped by the_post(). 
					?>
					
					</section>
					</div>
					
					<footer class="article-footer">
						<?php if(has_tag()){ ?>
							<h3><?php _e('Từ khóa cho bạn', 'harpersbazaar'); ?></h3>
							<?php the_tags('<ul class="tags"><li>','</li><li>','</li></ul>'); ?>
						<?php } ?>
					</footer> <!-- end article footer -->
					<div id="article-comments">
						<h3><?php _e("Bình luận", 'harpersbazaar'); ?></h3>
						<?php comments_template(); ?>
					</div>
				</div>

				<?php endwhile; ?>

				<?php else : ?>

					<article id="post-not-found" class="hentry clearfix">
						<header class="article-header">
							<h1><?php _e("Không tìm thấy bài viết!", "harpersbazaar"); ?></h1>
						</header>
					</article>
				<?php endif; ?>

				<?php get_sidebar(); ?>

			</div> <!-- end .page-wrapper -->

		</div> <!-- end #main -->
	</div> <!-- end .container -->
</div> <!-- end #content -->

<?php get_footer(); ?>
