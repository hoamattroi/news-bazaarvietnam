			<?php get_template_part("parts/bazaar-tv"); ?>

			<footer class="footer" role="contentinfo">
				<div id="inner-footer" class="container clearfix">
					<div id="inner_footer_row">
					<div id="issue_frontcover">
						<img src="<?php if(get_field('magazine_cover', 'option')){ echo getImgServerUrl(get_field('magazine_cover', 'option')); } ?>" alt="<?php bloginfo( 'name' ) ?>" title="<?php bloginfo( 'name' ) ?>" />
					</div>
					<div id="inner_footer_wrapper">
						<div id="magazine_title">
							<div id="magazine_desc">
								<?php if ( get_field('magazine_title','option') ): ?>
									<h4><?php echo apply_filters( 'the_title', get_field('magazine_title','option') ); ?></h4>
								<?php else : ?>
									<h4><?php _e('Tạp chí', 'harpersbazaar'); ?></h4>
								<?php endif ?>
								
								<?php if ( get_field('magazine_caption','option') ): ?>
									<h3 itemprop="publisher"><?php echo apply_filters( 'the_title', get_field('magazine_caption','option') ); ?></h3>
								<?php else : ?>
									<h3 itemprop="publisher"><?php _e('harper’s bazaar việt nam', 'harpersbazaar'); ?></h3>
								<?php endif ?>
							</div>
							<div id="past_issues">
								<!-- div class="footer_button">
									<a href="#footerbutton"><?php _e('Xem các số báo trước', 'harpersbazaar'); ?></a>
								</div -->
							</div>
						</div>
						<nav role="navigation">
							<?php bones_footer_links(); ?>
						</nav>
						<div id="social_interaction">
							<div id="integration_wrapper">
								<p><p>
								<h3<br/></h3>
								
							</div>
							<div id="social_wrapper">
								<div>
									<a class="social-icon social-icon-facebook" href="https://www.facebook.com/HarpersBazaarVN">Facebook</a>
									<a class="social-icon social-icon-pinterest" href="https://www.pinterest.com">Pinterest</a>
									<a class="social-icon social-icon-gplus" href="https://plus.google.com/103707460003962033216/">Google Plus</a>
									<a class="social-icon social-icon-youtube" href="http://www.youtube.com/user/HarpersBazaarVN">YouTube</a>
								</div> 
								<p><?php _e("CÔNG TY TNHH TRUYỀN THÔNG HOA MẶT TRỜI<br />
&copy; Copyright Harper's Bazaar Việt Nam, All rights reserved<br />
Số 190 Nguyễn Văn Hưởng, P. Thảo Điền, Q2, TP.HCM<br />
(+84 8) 3519 2301", "harpersbazaar"); ?><br/>Phiên bản thử nghiệm Beta</p>
<a href="http://www.dmca.com/Protection/Status.aspx?ID=6f725660-e52e-4afe-ac9e-1acc6211415c" title="DMCA.com Protection Status" class="dmca-badge"> <img src="//images.dmca.com/Badges/DMCA_logo-green150w.png?ID=6f725660-e52e-4afe-ac9e-1acc6211415c" alt="DMCA.com Protection Status"></a> <script src="//images.dmca.com/Badges/DMCABadgeHelper.min.js"> </script>
							</div>
						</div>
					</div>
					</div>
				</div> <!-- end #inner-footer -->

			</footer> <!-- end footer -->
			<div id="popupDialog">
			<div id="back_to_top_button"><a href="#" class="back-to-top">&nbsp;</a></div>
				<a id="popupDialog-close" href="javascript:;">x</a>
				<div class="content">
					<div>
						
						<?php if ( is_front_page() ): ?>

						<script type='text/javascript'><!--//<![CDATA[
						   var m3_u = (location.protocol=='https:'?'https://ad.hoamattroi.com/www/delivery/ajs.php':'http://ad.hoamattroi.com/www/delivery/ajs.php');
						   var m3_r = Math.floor(Math.random()*99999999999);
						   if (!document.MAX_used) document.MAX_used = ',';
						   document.write ("<scr"+"ipt type='text/javascript' src='"+m3_u);
						   document.write ("?zoneid=128006");
						   document.write ('&amp;cb=' + m3_r);
						   if (document.MAX_used != ',') document.write ("&amp;exclude=" + document.MAX_used);
						   document.write (document.charset ? '&amp;charset='+document.charset : (document.characterSet ? '&amp;charset='+document.characterSet : ''));
						   document.write ("&amp;loc=" + escape(window.location));
						   if (document.referrer) document.write ("&amp;referer=" + escape(document.referrer));
						   if (document.context) document.write ("&context=" + escape(document.context));
						   if (document.mmm_fo) document.write ("&amp;mmm_fo=1");
						   document.write ("'><\/scr"+"ipt>");
						//]]>--></script><noscript><a href='http://ad.hoamattroi.com/www/delivery/ck.php?n=a6ef6e81&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://ad.hoamattroi.com/www/delivery/avw.php?zoneid=128006&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=a6ef6e81' border='0' alt='' /></a></noscript>
							
						<?php else: ?>

						<script type='text/javascript'><!--//<![CDATA[
						   var m3_u = (location.protocol=='https:'?'https://ad.hoamattroi.com/www/delivery/ajs.php':'http://ad.hoamattroi.com/www/delivery/ajs.php');
						   var m3_r = Math.floor(Math.random()*99999999999);
						   if (!document.MAX_used) document.MAX_used = ',';
						   document.write ("<scr"+"ipt type='text/javascript' src='"+m3_u);
						   document.write ("?zoneid=128004");
						   document.write ('&amp;cb=' + m3_r);
						   if (document.MAX_used != ',') document.write ("&amp;exclude=" + document.MAX_used);
						   document.write (document.charset ? '&amp;charset='+document.charset : (document.characterSet ? '&amp;charset='+document.characterSet : ''));
						   document.write ("&amp;loc=" + escape(window.location));
						   if (document.referrer) document.write ("&amp;referer=" + escape(document.referrer));
						   if (document.context) document.write ("&context=" + escape(document.context));
						   if (document.mmm_fo) document.write ("&amp;mmm_fo=1");
						   document.write ("'><\/scr"+"ipt>");
						//]]>--></script><noscript><a href='http://ad.hoamattroi.com/www/delivery/ck.php?n=a1010a6b&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://ad.hoamattroi.com/www/delivery/avw.php?zoneid=128004&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=a1010a6b' border='0' alt='' /></a></noscript>

						<?php endif ?>
						
					</div>
				</div>
			</div>
		</div> <!-- end #container -->

		<!-- all js scripts are loaded in library/bones.php -->
		<?php wp_footer(); ?>
		<!--<script type="text/javascript" async src="//assets.pinterest.com/js/pinit.js"></script>-->
	</body>

</html> <!-- end page. what a ride! -->