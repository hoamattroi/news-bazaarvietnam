<?php get_header(); ?>

<div id="content">
	<div class="container">
		<div id="main" class="clearfix" role="main">

			<div class="article-pre">
				<?php if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p class="breadcrumbs">','</p>');
				} ?>
			</div>

			<div class="page-wrapper">

				<div class="category-content-wrapper">
	
					<?php //get_template_part("parts/trends-latest"); ?>

					<?php 
						$terms = get_terms( array(
						    'taxonomy' => 'collection',
						    'hide_empty' => false,
						    'order'	=> 'DESC',
							'orderby' => 'title',
							'parent' => 0,
							) 
						);
						$cur_term_id = get_queried_object()->term_id;
						$child_name = get_queried_object()->name;
					?>
					<ol class="snap_nav">
						<li><a href="<?php echo get_term_link( 17 ); ?>"><?php _e('All','harpersbazaar'); ?></a></li>
						<?php foreach ($terms as $key => $term): ?>
							<?php if ( $cur_term_id === $term->term_id ){ ?>
								<li class="snap_selected" ><?php echo $term->name; ?></li>
							<?php }else{ ?>
								<li><a href="<?php echo get_term_link( $term->term_id, 'collection' ); ?>"><?php echo $term->name; ?></a></li>
							<?php } ?>
						<?php endforeach ?>
					</ol>

					<div class="row">
						<?php if (have_posts()) : ?>
							<?php $term_childs = get_term_children( $cur_term_id, 'collection' ); ?>
							<?php if ( $term_childs ){ ?>
								<?php foreach ($term_childs as $key => $child): ?>
									<?php 
										$term = get_term( $child, 'collection' );
										$child_name = $term->name;
									?>
									<?php 
									$args = array(
										//Type & Status Parameters
										'posts_per_page' => 6,
										'post_type'   => 'post',
										'post_status' => 'publish',
										//Order & Orderby Parameters
										'order'               => 'DESC',
										'orderby'             => 'date',
										//Tag Parameters
										'tax_query' => array(
											'relation' => 'AND',
											array(
												'taxonomy' => 'collection',
												'field'    => 'term_id',
												'terms'    => array( $child ),
												// 'operator' => 'NOT IN',
											),
										),
									);
									
									$posts_child = new WP_Query( $args ); 

									if ($posts_child->have_posts()) { ?>

										<a href="<?php echo get_term_link($child, 'collection'); ?>"><h1 class="category-title barred-heading"><span><?php echo apply_filters( 'the_title', $child_name ); ?></span></h1></a>

										<?php while ($posts_child->have_posts()) : $posts_child->the_post(); ?>

											<?php get_template_part('parts/tax-article-content'); ?>

										<?php endwhile; ?>
									<?php } ?>

								<?php endforeach ?>

							<?php }elseif ( $cur_term_id ) { ?>

								<?php if (have_posts()) { ?>
									<h1 class="category-title barred-heading"><span><?php echo apply_filters( 'the_title', $child_name ); ?></span></h1>
									<?php while (have_posts()) : the_post(); ?>

										<?php get_template_part('parts/tax-article-content'); ?>

									<?php endwhile; ?>

									<?php if (function_exists('bones_page_navi')) { ?>
										<?php bones_page_navi(); ?>
									<?php } else { ?>
										<nav class="wp-prev-next">
											<ul class="clearfix">
												<li class="prev-link"><?php next_posts_link(__('&laquo; Older Entries', "harpersbazaar")) ?></li>
												<li class="next-link"><?php previous_posts_link(__('Newer Entries &raquo;', "harpersbazaar")) ?></li>
											</ul>
										</nav>
									<?php } ?>

								<?php } ?>

							<?php } ?>

						<?php else : ?>

							<div id="post-not-found" class="hentry clearfix">
								<header class="article-header">
									<h1><?php _e("Chưa có bài viết trong chuyên mục này", "harpersbazaar"); ?></h1>
								</header>
							</div>

						<?php endif; ?>
					</div> <!-- .row -->

				</div> <!-- .category-content-wrapper -->

				<?php get_sidebar(); ?>

			</div> <!-- end .page-wrapper -->

		</div> <!-- end #main -->
	</div> <!-- end .container -->

</div> <!-- end #content -->

<section class="tags moremore">
	<h2 class="more-heading"><span><?php _e("Xem thêm...", "harpersbazaar"); ?></span></h2>
	<ul class=more_tags><?php 
		$curr_cat = get_cat_id( single_cat_title("",false) );
		top_tags($curr_cat); 
		?></ul>
</section>

<?php get_footer(); ?>
