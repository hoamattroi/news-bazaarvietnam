<?php get_header(); ?>

			<div id="content">
	<div class="container">
		<div id="main" class="clearfix" role="main">

			<div class="article-pre">
				<?php if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p class="breadcrumbs">','</p>');
				} ?>
			</div>

			<div class="page-wrapper">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<div class="article-wrapper">
					<article class="article-primary" id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/Article">

						<header class="article-header">
							<h1 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h1>
							<?php
								$myExcerpt = get_the_excerpt();
								$tags = array("<p>", "</p>");
								$myExcerpt = str_replace($tags, "", $myExcerpt);
  
							?>
							<p class="standfirst"><?php echo $myExcerpt; ?></p>
							<div class="byline vcard clearfix">
								<div class="authorship">
									<?php _e("Do", 'harpersbazaar'); ?>
									<span class="author" itemprop="author"><?php the_author_posts_link(); ?></span>
									<?php _e("đăng ngày", 'harpersbazaar'); ?>
									<time class="time" pubdate itemprop="datePublished"><?php the_date('d-m-Y'); ?></time>
								</div>
							</div>

						</header> <!-- end article header -->

						<section class="entry-content clearfix" itemprop="articleBody">

							<?php 
								if(get_field('tabbed-slideshow')) {
									get_template_part("parts/slideshow-tabbed");
								} elseif(get_field('video_or_youtube_link')){
									$videolink = get_field('video_or_youtube_link');
									if(get_field('youtube_or_local')=='Local'){
										echo JWP6_Shortcode::the_content_filter('[jwplayer mediaid="'.$videolink.'"]');
									} else {
										echo '<div class="video-container" itemprop="video"><iframe id="ytplayer" type="text/html"
  											src="http://www.youtube.com/embed/'.$videolink.'?autoplay=1" width="560" height="315" frameborder="0"/></div>';
									}
								} else {
									the_post_thumbnail( 'full');
								}
							?>
							
							<?php the_content(); ?>

							<div class="fb-like" data-width="400" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div>
						</section> <!-- end article section -->
						
					
						
				</article> <!-- end article -->

					<div id="article-comments">
						<h3><?php _e("Bình luận", 'harpersbazaar'); ?></h3>
						<?php comments_template(); ?>
					</div>
				</div>

				<?php endwhile; ?>

				<?php else : ?>

					<article id="post-not-found" class="hentry clearfix">
						<header class="article-header">
							<h1><?php _e("Oops, Post Not Found!", "harpersbazaar"); ?></h1>
						</header>
						<section class="entry-content">
							<p><?php _e("Uh Oh. Something is missing. Try double checking things.", "harpersbazaar"); ?></p>
						</section>
						<footer class="article-footer">
								<p><?php _e("This is the error message in the single.php template.", "harpersbazaar"); ?></p>
						</footer>
					</article>
				<?php endif; ?>

				<?php get_sidebar(); ?>

			</div> <!-- end .page-wrapper -->

		</div> <!-- end #main -->
	</div> <!-- end .container -->
</div> <!-- end #content -->

<?php get_footer(); ?>
