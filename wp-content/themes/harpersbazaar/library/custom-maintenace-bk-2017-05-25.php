<?php
	//The third parameter corresponds to action priority,
	//set it to change the order of execution in case of a conflict
	add_action('pre_get_posts', 'the_modified_loop', 10);

	function the_modified_loop($query){
	    //Remove 'is_admin' if you want the code to run for the backend category archive page
	    if( $query->is_main_query() && !is_admin() && ($query->is_category() || $query->is_archive() || $query->is_home() || $query->is_front_page()) ) {
	    	if ($query->query_vars['paged'] == 1 || $query->query_vars['paged'] == 0) {
		        $query->set( 'offset', 3 );
	    	}
	    }

	    // if( $query->is_main_query() && !is_admin() && ($query->is_category('runway') )) {
	        // $query->set( 'offset', 0 );
	        // $query->set( 'posts_per_page', 6);
	    // }
	}


	if ( ! function_exists( 'reg_custom_post_type' ) ) {
		function reg_custom_post_type() { 
			register_post_type('tap-chi', array(
					'label' => 'Tạp chí',
					'description' => 'Tạp chí Harper\'s Bazaar',
					'public' => true,
					'show_ui' => true,
					'show_in_menu' => true,
					'capability_type' => 'post',
					'map_meta_cap' => true,
					'hierarchical' => false,
					'rewrite' => array('slug' => 'tap-chi', 'with_front' => true),
					'query_var' => true,
					'has_archive' => true,
					'taxonomies' => array('post_tag'),
					'supports' => array('title','editor','excerpt','revisions','thumbnail','author'),
					'labels' => array (
					  'name' => 'Tạp chí',
					  'singular_name' => 'Tạp chí',
					  'menu_name' => 'Tạp chí',
					  'add_new' => 'Add Tạp chí',
					  'add_new_item' => 'Add New Tạp chí',
					  'edit' => 'Edit',
					  'edit_item' => 'Edit Tạp chí',
					  'new_item' => 'New Tạp chí',
					  'view' => 'View Tạp chí',
					  'view_item' => 'View Tạp chí',
					  'search_items' => 'Search Tạp chí',
					  'not_found' => 'No Tạp chí Found',
					  'not_found_in_trash' => 'No Tạp chí Found in Trash',
					  'parent' => 'Parent Tạp chí',
					)
				) 
			);
			//Register Taxonomy
			register_taxonomy( 'year', array( 'tap-chi' ), array( 
					'hierarchical' => true, 
					'query_var' => true,
					'show_ui' => true,
					'show_in_nav_menus' => false,
					'label' => __('Year')
				)
			);
			//Register Taxonomy
			register_taxonomy( 'collection', array( 'post' ), array( 
					'hierarchical' => true, 
					'query_var' => true,
					'show_ui' => true,
					'show_in_nav_menus' => false,
					'label' => __('Collection')
				)
			);
		}
		add_action( 'init', 'reg_custom_post_type' );
	}

	function stripVN($str) {
	    $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
	    $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
	    $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
	    $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
	    $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
	    $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
	    $str = preg_replace("/(đ)/", 'd', $str);

	    $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
	    $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
	    $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
	    $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
	    $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
	    $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
	    $str = preg_replace("/(Đ)/", 'D', $str);
	    return $str;
	}

