<?php



global $leaderboard;
global $halfpage;
global $mpu;
global $featured_brand_ad;

/******* LEADERBOARD **********/

if( is_home() || is_front_page()) {
	if(get_field('home_leaderboard', 'option')){
		if(have_rows('home_leaderboard', 'option')){
			while ( have_rows('home_leaderboard', 'option') ) : the_row(); 
				echo get_sub_field('home_leaderboard_code_header', 'option');
				$leaderboard = get_sub_field('home_leaderboard_header_ad_acode', 'option');
			endwhile;
		}
	}
} elseif(is_tax()){
	$taxonomy = get_query_var( 'taxonomy' );
	$queried_object = $wp_query->get_queried_object();
	$term_id = $queried_object->term_id;
	if(get_field('category_leaderboard',$term_id) || get_field('category_leaderboard','option')){
		if(have_rows('category_leaderboard',$term_id)){
			while ( have_rows('category_leaderboard',$term_id) ) : the_row(); 
				echo get_sub_field('cat_leaderboard_code_header',$term_id);
				$leaderboard = get_sub_field('category_leaderboard_ad_code',$term_id);
			endwhile;
		} elseif(have_rows('category_leaderboard', 'option')) {
			while ( have_rows('category_leaderboard', 'option') ) : the_row(); 
				echo get_sub_field('cat_leaderboard_code_header', 'option');
				$leaderboard = get_sub_field('category_leaderboard_ad_code','option');
			endwhile;
		}
	}
} elseif(is_category()){
	$cat_obj = get_queried_object_id();
	if(get_field('category_leaderboard',$cat_obj) || get_field('category_leaderboard','option')){
		if(have_rows('category_leaderboard',$cat_obj)){
			while ( have_rows('category_leaderboard',$cat_obj) ) : the_row(); 
				echo get_sub_field('cat_leaderboard_code_header',$cat_obj);
				$leaderboard = get_sub_field('category_leaderboard_ad_code',$cat_obj);
			endwhile;
		} elseif(have_rows('category_leaderboard', 'option')) {
			while ( have_rows('category_leaderboard', 'option') ) : the_row(); 
				echo get_sub_field('cat_leaderboard_code_header', 'option');
				$leaderboard = get_sub_field('category_leaderboard_ad_code','option');
			endwhile;
		}
	}

} elseif(get_post_type() == 'thuong-hieu'){
	if(get_field('brand_leaderboard', 'option')){
		if(have_rows('brand_leaderboard', 'option')){ 
			while ( have_rows('brand_leaderboard', 'option') ) : the_row(); 
				echo get_sub_field('brand_leaderboard_code_header', 'option');
				$leaderboard = get_sub_field('brand_leaderboard_ad_code', 'option');
			endwhile;
		}
	}
} elseif(get_post_type() == 'chan-dung'){
	if(get_field('people', 'option')){
		if(have_rows('people', 'option')){ 
			while ( have_rows('people', 'option') ) : the_row(); 
				echo get_sub_field('people_leaderboard_code_header', 'option');
				$leaderboard = get_sub_field('people_leaderboard_ad_code', 'option');
			endwhile;
		}
	}
} else{
	if(get_field('post_leaderboard', 'option')){
		if(have_rows('post_leaderboard', 'option')){
			while ( have_rows('post_leaderboard', 'option') ) : the_row(); 
				echo get_sub_field('post_leaderboard_header_code', 'option');
				$leaderboard = get_sub_field('post_leaderboard_ad_code', 'option');
			endwhile;
		}
	}
}


/******************************/





/********** Halfpage **********/

if(is_tax()){
	$taxonomy = get_query_var( 'taxonomy' );
	$queried_object = $wp_query->get_queried_object();
	$term_id = $queried_object->term_id;
	if(get_field('cat_sidebar_halfpage',$term_id) || get_field('cat_sidebar_halfpage','option')){
		if(have_rows('cat_sidebar_halfpage',$term_id)){
			while ( have_rows('cat_sidebar_halfpage',$term_id) ) : the_row(); 
				echo get_sub_field('cat_halfpage_code_header',$term_id);
				$halfpage .= get_sub_field('category_halfpage_ad_code',$term_id);
			endwhile;
		} elseif(have_rows('cat_sidebar_halfpage', 'option')) {
			while ( have_rows('cat_sidebar_halfpage', 'option') ) : the_row(); 
				echo get_sub_field('cat_halfpage_code_header', 'option');
				$halfpage .= get_sub_field('category_halfpage_ad_code','option');
			endwhile;
		}
	}
} elseif(is_category()){
	$cat_obj = get_queried_object_id();
	if(get_field('cat_sidebar_halfpage',$cat_obj) || get_field('cat_sidebar_halfpage','option')){
		if(have_rows('cat_sidebar_halfpage',$cat_obj)){
			while ( have_rows('cat_sidebar_halfpage',$cat_obj) ) : the_row(); 
				echo get_sub_field('cat_halfpage_code_header',$cat_obj);
				$halfpage .= get_sub_field('category_halfpage_ad_code',$cat_obj);
			endwhile;
		} elseif(have_rows('cat_sidebar_halfpage', 'option')) {
			while ( have_rows('cat_sidebar_halfpage', 'option') ) : the_row(); 
				echo get_sub_field('cat_halfpage_code_header', 'option');
				$halfpage .= get_sub_field('category_halfpage_ad_code','option');
			endwhile;
		}
	}

} elseif(get_post_type() == 'thuong-hieu'){
	if(get_field('brand_halfpage', 'option')){
		if(have_rows('brand_halfpage', 'option')){ 
			while ( have_rows('brand_halfpage', 'option') ) : the_row(); 
				echo get_sub_field('brand_halfpage_code_header', 'option');
				$halfpage .= get_sub_field('brand_halfpage_ad_code', 'option');
			endwhile;
		}
	}
} elseif(get_post_type() == 'chan-dung'){
	if(get_field('people_halfpage', 'option')){
		if(have_rows('people_halfpage', 'option')){ 
			while ( have_rows('people_halfpage', 'option') ) : the_row(); 
				echo get_sub_field('people_halfpage_code_header', 'option');
				$halfpage = get_sub_field('people_halfpage_ad_code', 'option');
			endwhile;
		}
	}
} elseif(get_post_type() == 'post'){
	if(get_field('post_halfpage', 'option')){
		if(have_rows('post_halfpage', 'option')){
			while ( have_rows('post_halfpage', 'option') ) : the_row(); 
				echo get_sub_field('post_halfpage_header_code', 'option');
				$halfpage .= get_sub_field('post_halfpage_ad_code', 'option');
			endwhile;
		}
	}
}



/******************************/



/*********** MPU's ************/


if( is_home() || is_front_page()) {
	if(get_field('home_mpu', 'option')){
		if(have_rows('home_mpu', 'option')){
			while ( have_rows('home_mpu', 'option') ) : the_row(); 
				echo get_sub_field('home_mpu_code_header', 'option');
			endwhile;
		}
	}
}elseif(is_tax()){
	$taxonomy = get_query_var( 'taxonomy' );
	$queried_object = $wp_query->get_queried_object();
	$term_id = $queried_object->term_id;
	if(get_field('category_sidebar_mpu',$term_id) || get_field('category_sidebar_mpu','option')){
		if(have_rows('category_sidebar_mpu',$term_id)){
			while ( have_rows('category_sidebar_mpu',$term_id) ) : the_row(); 
				echo get_sub_field('category_mpu_code_header',$term_id);
				$mpu = get_sub_field('category_mpu_ad_code',$term_id);
			endwhile;
		} elseif(have_rows('category_sidebar_mpu', 'option')) {
			while ( have_rows('category_sidebar_mpu', 'option') ) : the_row(); 
				echo get_sub_field('category_mpu_code_header', 'option');
				$mpu = get_sub_field('category_mpu_ad_code','option');
			endwhile;
		}
	}
} elseif(is_category()){
	$cat_obj = get_queried_object_id();
	if(get_field('category_sidebar_mpu',$cat_obj) || get_field('category_sidebar_mpu','option')){
		if(have_rows('category_sidebar_mpu',$cat_obj)){
			while ( have_rows('category_sidebar_mpu',$cat_obj) ) : the_row(); 
				echo get_sub_field('category_mpu_code_header',$cat_obj);
				$mpu = get_sub_field('category_mpu_ad_code',$cat_obj);
			endwhile;
		} elseif(have_rows('category_sidebar_mpu', 'option')) {
			while ( have_rows('category_sidebar_mpu', 'option') ) : the_row(); 
				echo get_sub_field('category_mpu_code_header', 'option');
				$mpu = get_sub_field('category_mpu_ad_code','option');
			endwhile;
		}
	}
} elseif(get_post_type() == 'post'){
	if(get_field('post_sidebar_mpu', 'option')){
		if(have_rows('post_sidebar_mpu', 'option')){
			while ( have_rows('post_sidebar_mpu', 'option') ) : the_row(); 
				echo get_sub_field('post_mpu_header_code', 'option');
				$mpu = get_sub_field('post_mpu_ad_code', 'option');
			endwhile;
		}
	}
}

/******************************/
if(is_category()){
	if(have_rows('featured_brand', 'option')) {
		while ( have_rows('featured_brand', 'option') ) : the_row(); 
			echo get_sub_field('featured_brand_header', 'option');
			$featured_brand_ad = get_sub_field('featured_brand_ad_code','option');
		endwhile;
	}
}

if(get_post_type() == 'post'){
	if(get_field('post_sidebar_mpu', 'option')){
		if(have_rows('post_sidebar_mpu', 'option')){
			while ( have_rows('post_sidebar_mpu', 'option') ) : the_row(); 
				echo get_sub_field('post_mpu_header_code', 'option');
				$mpu = get_sub_field('post_mpu_ad_code', 'option');
			endwhile;
		}
	}
}
?>