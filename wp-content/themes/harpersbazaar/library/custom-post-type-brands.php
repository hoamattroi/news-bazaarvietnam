<?php

add_action('init', 'cptui_register_my_cpt_chan_dung');
function cptui_register_my_cpt_chan_dung() {
register_post_type('thuong-hieu', array(
'label' => 'Thương hiệu',
'description' => 'Fashion Brand',
'public' => true,
'show_ui' => true,
'show_in_menu' => true,
'capability_type' => 'post',
'map_meta_cap' => true,
'hierarchical' => false,
'rewrite' => array('slug' => 'thuong-hieu', 'with_front' => true),
'query_var' => true,
'has_archive' => true,
'taxonomies' => array('post_tag'),
'supports' => array('title','editor','excerpt','trackbacks','custom-fields','comments','revisions','thumbnail','author','page-attributes','post-formats',),
'labels' => array (
  'name' => 'Thương hiệu',
  'singular_name' => 'Thương hiệu',
  'menu_name' => 'Thương hiệu',
  'add_new' => 'Add Thương hiệu',
  'add_new_item' => 'Add New Thương hiệu',
  'edit' => 'Edit',
  'edit_item' => 'Edit Thương hiệu',
  'new_item' => 'New Thương hiệu',
  'view' => 'View Thương hiệu',
  'view_item' => 'View Thương hiệu',
  'search_items' => 'Search Thương hiệu',
  'not_found' => 'No Thương hiệu Found',
  'not_found_in_trash' => 'No Thương hiệu Found in Trash',
  'parent' => 'Parent Thương hiệu',
)
) ); }

add_action('init', 'cptui_register_my_taxes_brand_category');
function cptui_register_my_taxes_brand_category() {
register_taxonomy( 'brand-category',array (
  0 => 'thuong-hieu',
),
array( 'hierarchical' => true,
	'label' => 'Brand Categories',
	'show_ui' => true,
	'query_var' => true,
	'show_admin_column' => false,
	'labels' => array (
  'search_items' => 'Brand Category',
  'popular_items' => '',
  'all_items' => '',
  'parent_item' => '',
  'parent_item_colon' => '',
  'edit_item' => '',
  'update_item' => '',
  'add_new_item' => '',
  'new_item_name' => '',
  'separate_items_with_commas' => '',
  'add_or_remove_items' => '',
  'choose_from_most_used' => '',
)
) ); 
}

?>