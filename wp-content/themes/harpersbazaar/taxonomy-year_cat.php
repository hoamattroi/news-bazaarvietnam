<?php get_header(); ?>

<div id="content">
	<div class="container">
		<div id="main" class="clearfix" role="main">

			<div class="article-pre">
				<?php if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p class="breadcrumbs">','</p>');
				} ?>
			</div>

			<div class="page-wrapper">

				<div class="category-content-wrapper">
	
					<h1 class="category-title barred-heading">
						<span><?php single_cat_title(); ?></span>
					</h1>
					<?php 
						$curr_cat = get_queried_object()->term_id;
						
							/**
							 * The WordPress Query class.
							 * @link http://codex.wordpress.org/Function_Reference/WP_Query
							 *
							 */
							$args = array(
								//Type & Status Parameters
								'post_type'   => 'tap-chi',
								'post_status' => 'publish',
								
								//Order & Orderby Parameters
								'order'               => 'DESC',
								'orderby'             => 'date',
								'ignore_sticky_posts' => true,
								//Pagination Parameters
								// 'posts_per_page'         => 10,
								// 'posts_per_archive_page' => 10,
								'tax_query' => array(
									'relation'  => 'AND',
									array(
										'taxonomy'         => 'year_cat',
										'field'            => 'id',
										'terms'            => array( $curr_cat ),
										'include_children' => false,
										// 'operator'         => 'NOT IN'
									)
								),
							);
						
						$loop = new WP_Query( $args );
						
					?>
					<?php //$postCount = 1; ?>
					<?php if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
						<?php //$postCount++; ?>

						<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">

							<header class="article-header">

								<a href="<?php the_permalink() ?>"><?php the_post_thumbnail('square-16x9'); ?></a>

								<h2 class="h2"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>

							</header> <!-- end article header -->

						</article> <!-- end article -->

					<?php endwhile; ?>

					<?php if (function_exists('bones_page_navi')) { ?>
						<?php bones_page_navi(); ?>
					<?php } else { ?>
						<nav class="wp-prev-next">
							<ul class="clearfix">
								<li class="prev-link"><?php next_posts_link(__('&laquo; Older Entries', "harpersbazaar")) ?></li>
								<li class="next-link"><?php previous_posts_link(__('Newer Entries &raquo;', "harpersbazaar")) ?></li>
							</ul>
						</nav>
					<?php } ?>

					<?php else : ?>

						<div id="post-not-found" class="hentry clearfix">
							<header class="article-header">
								<h1><?php _e("Chưa có bài viết trong chuyên mục này", "harpersbazaar"); ?></h1>
							</header>
						</div>
						
					<?php endif; ?>

				</div> <!-- .category-content-wrapper -->

				<?php get_sidebar(); ?>

			</div> <!-- end .page-wrapper -->

			 

		</div> <!-- end #main -->
	</div> <!-- end .container -->
</div> <!-- end #content -->
<section class="tags moremore">
	<h2 class="more-heading"><span><?php _e("Xem thêm...", "harpersbazaar"); ?></span></h2>
	<ul class=more_tags><?php
		top_tags($curr_cat); 
		?></ul>
</section>
<?php get_footer(); ?>
