<?php
/*
YARPP Template: Thumbnails
Description: Requires a theme which supports post thumbnails
Author: mitcho (Michael Yoshitaka Erlewine)
*/ ?>

<section class="related-posts">
<h3><?php _e("You Might Like", 'harpersbazaar'); ?>:</h3>
<?php if (have_posts()):?>
	<?php while (have_posts()) : the_post(); ?>
		<?php if (has_post_thumbnail()):?>
		<article>
			<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
				<?php the_post_thumbnail('latest-article-thumb'); ?>
				<?php the_title(); ?>
			</a>
		</article>
		<?php endif; ?>
	<?php endwhile; ?>
<?php else: ?>
<p><?php _e("No related posts.", 'harpersbazaar'); ?></p>
<?php endif; ?>
</section>