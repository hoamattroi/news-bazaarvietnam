<?php
/*
Author: Eddie Machado
URL: htp://themble.com/bones/

This is where you can drop your custom functions or
just edit things like thumbnail sizes, header images,
sidebars, comments, ect.
*/

/************* INCLUDE NEEDED FILES ***************/

/*
1. library/bones.php
	- head cleanup (remove rsd, uri links, junk css, ect)
	- enqueueing scripts & styles
	- theme support functions
	- custom menu output & fallbacks
	- related post function
	- page-navi function
	- removing <p> from around images
	- customizing the post excerpt
	- custom google+ integration
	- adding custom fields to user profiles
*/
require_once('library/bones.php'); // if you remove this, bones will break

/*
2. library/custom-post-type.php
	- an example custom post type
	- example custom taxonomy (like categories)
	- example custom taxonomy (like tags)
*/
require_once('library/custom-post-type-trends.php');
require_once('library/custom-post-type-brands.php');
require_once('library/custom-post-type-people.php');
require_once('library/custom-post-type-runway.php');
require_once('library/custom-calendar.php');
require_once('library/custom-maintenace.php');

if ( IMG_SERVER )
	define('IMG_SERVER', 'http://images.bazaarvietnam.vn');

/*
3. library/admin.php
	- removing some default WordPress dashboard widgets
	- an example custom dashboard widget
	- adding custom login css
	- changing text in footer of admin
*/
// require_once('library/admin.php'); // this comes turned off by default
/*
4. library/translation/translation.php
	- adding support for other languages
*/
// require_once('library/translation/translation.php'); // this comes turned off by default

/************* THUMBNAIL SIZE OPTIONS *************/

// Thumbnail sizes
add_image_size( 'bones-thumb-600', 600, 150, true );
add_image_size( 'bones-thumb-300', 300, 100, true );
add_image_size( 'square-360', 360, 360, true );
add_image_size( 'square-720', 720, 720, true );
add_image_size( 'latest-article-thumb', 440, 400, true );
add_image_size( 'thumb-300x410', 300, 410, true );
add_image_size( 'thumb-310x170', 310, 170, true );
add_image_size( 'slideshow-portrait', 340, 510, true );
add_image_size( 'slideshow-landscape', 640, 400, true );
add_image_size( 'thumb-brand-people', 186, 186, true );
add_image_size( 'brandlarge', 1100, 600, true );
add_image_size( 'featured-person', 250, 320, true );
add_image_size( 'runway_thumbnail', 305, 450, true);
add_image_size( 'runway_category', 150, 205, true);
add_image_size( 'thumb-16x9', 160, 90, true);
add_image_size( 'square-16x9', 320, 180, true);
add_image_size( 'normal-16x9', 480, 270, true);
add_image_size( 'large-16x9', 720, 405, true);

/*
to add more sizes, simply copy a line from above
and change the dimensions & name. As long as you
upload a "featured image" as large as the biggest
set width or height, all the other sizes will be
auto-cropped.

To call a different size, simply change the text
inside the thumbnail function.

For example, to call the 300 x 300 sized image,
we would use the function:
<?php the_post_thumbnail( 'bones-thumb-300' ); ?>
for the 600 x 100 image:
<?php the_post_thumbnail( 'bones-thumb-600' ); ?>

You can change the names and dimensions to whatever
you like. Enjoy!
*/



/*$fb_page_id = "172235112912077";
$config = array(
	'appid'  => $fb_page_id,
	'secret' => $fb_access_token
);
https://graph.facebook.com/".$id."/feed&access_token=".$facebook_access_token

	'access_token' => $fb_access_token

$facebook = new Facebook($config);
$response = $facebook->api('/'.$fb_page_id.'/feed');
*/


/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function bones_register_sidebars() {
	register_sidebar(array(
		'id' => 'sidebar1',
		'name' => __('Right Column on Article page', 'harpersbazaar'),
		'description' => __('These items are displayed in the right column of the article page.', 'harpersbazaar'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	/*
	to add more sidebars or widgetized areas, just copy
	and edit the above sidebar code. In order to call
	your new sidebar just use the following code:

	Just change the name to whatever your new
	sidebar's id is, for example:

	register_sidebar(array(
		'id' => 'sidebar2',
		'name' => __('Sidebar 2', 'harpersbazaar'),
		'description' => __('The second (secondary) sidebar.', 'harpersbazaar'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	To call the sidebar in your template, you can just copy
	the sidebar.php file and rename it to your sidebar's name.
	So using the above example, it would be:
	sidebar-sidebar2.php

	*/
} // don't remove this bracket!

/************* COMMENT LAYOUT *********************/

// Comment Layout
function bones_comments($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
	<li <?php comment_class(); ?>>
		<article id="comment-<?php comment_ID(); ?>" class="clearfix">
			<header class="comment-author vcard">
				<?php
				/*
					this is the new responsive optimized comment image. It used the new HTML5 data-attribute to display comment gravatars on larger screens only. What this means is that on larger posts, mobile sites don't have a ton of requests for comment images. This makes load time incredibly fast! If you'd like to change it back, just replace it with the regular wordpress gravatar call:
					echo get_avatar($comment,$size='32',$default='<path_to_url>' );
				*/
				?>
				<!-- custom gravatar call -->
				<?php
					// create variable
					$bgauthemail = get_comment_author_email();
				?>
				<img data-gravatar="http://www.gravatar.com/avatar/<?php echo md5($bgauthemail); ?>?s=32" class="load-gravatar avatar avatar-48 photo" height="32" width="32" src="<?php echo get_template_directory_uri(); ?>/library/images/nothing.gif" />
				<!-- end custom gravatar call -->
				<?php printf(__('<cite class="fn">%s</cite>', 'harpersbazaar'), get_comment_author_link()) ?>
				<time datetime="<?php echo comment_time('Y-m-j'); ?>"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php comment_time(__('F jS, Y', 'harpersbazaar')); ?> </a></time>
				<?php edit_comment_link(__('(Edit)', 'harpersbazaar'),'  ','') ?>
			</header>
			<?php if ($comment->comment_approved == '0') : ?>
				<div class="alert alert-info">
					<p><?php _e('Your comment is awaiting moderation.', 'harpersbazaar') ?></p>
				</div>
			<?php endif; ?>
			<section class="comment_content clearfix">
				<?php comment_text() ?>
			</section>
			<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
		</article>
	<!-- </li> is added by WordPress automatically -->
<?php
} // don't remove this bracket!

/************* SEARCH FORM LAYOUT *****************/

// Search Form
function bones_wpsearch($form) {
	$form = '<form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
	<input class="search-text" type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="'.esc_attr__('Search here','harpersbazaar').'" />
	<input class="search-button" type="submit" id="searchsubmit" value="" />
	</form>';
	return $form;
} // don't remove this bracket!

add_action( 'init', 'my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}


// Custom Navi Walker

require_once('library/custom-walker.php');

// Advanced Custom Fields
include("library/acf.php"); /* exported fields code */
if ( function_exists('acf_add_options_sub_page') ) {
	acf_add_options_sub_page( 'Top Three' );
}
if ( function_exists('acf_add_options_sub_page') ) {
	acf_add_options_sub_page( 'Home Slider' );
}
if ( function_exists('acf_add_options_sub_page') ) {
	acf_add_options_sub_page( "Home Featured" );
}
if ( function_exists('acf_add_options_sub_page') ) {
	//acf_add_options_sub_page( "Harper's BAZAAR TV" );
}
if ( function_exists('acf_add_options_sub_page') ) {
	acf_add_options_sub_page( "Advertising" );
}

if ( function_exists('acf_add_options_sub_page') ) {
	acf_add_options_sub_page( "Magazine Cover" );
}

if ( function_exists('acf_add_options_sub_page') ) {
	acf_add_options_sub_page( "Search and Analytics Settings" );
}

if ( function_exists('acf_add_options_sub_page') ) {
	acf_add_options_sub_page( "Current Runway Season" );
}

add_filter('widget_text', 'do_shortcode');

// http://stackoverflow.com/questions/12391779/getting-google-plus-shares-for-a-given-url-in-php
function getGplusShares($url) {
	$buttonUrl = sprintf('https://plusone.google.com/u/0/_/+1/fastbutton?url=%s', urlencode($url));
	$htmlData  = file_get_contents($buttonUrl);

	@preg_match_all('#{c: (.*?),#si', $htmlData, $matches);
	$ret = isset($matches[1][0]) && strlen($matches[1][0]) > 0 ? trim($matches[1][0]) : 0;
	if(0 != $ret) {
		$ret = str_replace('.0', '', $ret);
	}

	return intval($ret);
}
function getFacebookShares($url) {
	$graphURL = sprintf('http://api.facebook.com/restserver.php?method=links.getStats&urls=%s&format=json', urlencode($url));
	$graphResponse = file_get_contents($graphURL);

	if ($graphResponse === FALSE) {
		return "";
	} else {
		$graphResponse = json_decode($graphResponse, true);
		$graphShares = $graphResponse[0]["share_count"]+$graphResponse[0]["like_count"];
		return $graphShares;
	}
}

function change_role_name() {
    global $wp_roles;

    if ( ! isset( $wp_roles ) )
        $wp_roles = new WP_Roles();

    //You can list all currently available roles like this...
    //$roles = $wp_roles->get_names();
    //print_r($roles);

    //You can replace "administrator" with any other role "editor", "author", "contributor" or "subscriber"...
    $wp_roles->roles['administrator']['name'] = 'Web Admin';
    $wp_roles->role_names['administrator'] = 'Web Admin';   
    
    $wp_roles->roles['contributor']['name'] = 'Journalist';
    $wp_roles->role_names['contributor'] = 'Journalist';   
    
    $wp_roles->roles['author']['name'] = 'Editor';
    $wp_roles->role_names['author'] = 'Editor';    
    
    $wp_roles->roles['editor']['name'] = 'Information Checker';
    $wp_roles->role_names['editor'] = 'Information Checker';          
}
add_action('init', 'change_role_name');

add_action('admin_head', 'admin_styles');
function admin_styles() {
  echo '<link rel="stylesheet" href="../wp-content/themes/harpersbazaar/library/css/admin.css" type="text/css" media="all" />';
}

/* Adds slideshow text in Advanced Custom Fields to Yoast's keyword analysis */
function yoast_analysis_acf( $content ) {
	global $post;

	$id = $post->ID;
	$customFields = get_post_custom($id);

	$slideText = "";

	foreach($customFields as $key => $value) {
		$currText = $value[0];
		if ( ((strpos($key, "slide_text") !== false) || (strpos($key, "tab_text") !== false) ) && (strpos($currText, "field_") === false) ) {
			$slideText .= $currText . "\n\n";
		}
	}

 	$content = $slideText . "\n\n" . $content;
	return $content;
}
add_filter( 'wpseo_pre_analysis_post_content', 'yoast_analysis_acf' );

function top_tags($category) {
	$category_posts = get_posts(array('category'=>$category) );
	
	foreach($category_posts as $post){
		$posttags = get_the_tags($post->ID);
		if ($posttags) {
			$f=0;
			foreach($posttags as $tag) {
				$all_tags_arr[$f] = $tag;
				$f++;
			}
		}
	}
	
	$counts = $tag_links = array();
	
	foreach ( (array) $all_tags_arr as $tag2 ) {
			$counts[$tag2->name] = $tag2->count;
			$all_tag_links[$tag2->name] = get_tag_link( $tag2->term_id );
	}
	
	asort($counts);
	
	$counts = array_reverse( $counts, true );
	$i = 0;
	foreach ( $counts as $tag3 => $count ) {
		$i++;
		$tag_link = $all_tag_links[$tag3];
		$tag3 = str_replace(' ', '&nbsp;', esc_html( $tag3 ));
		if($i<5){
			print "<li><a href=\"$tag_link\">$tag3</a></li>";
		}
	}
	
}

add_filter( 'human_time_diff' , 'my_human_time_diff' );


function my_human_time_diff( $from, $to = '' ) {
	if ( empty($to) ) $to = time();
		$diff = (int) abs($to - $from); /* mostra segundos */
		if ($diff <= 60) {
			$seconds = round($diff / 1);
			if ($seconds <= 1) {
				$seconds = 1;
			}
			$since = sprintf(_n('%s giây', '%s giây', $seconds), $seconds);
		} else if ($diff <= 3600) {
			$mins = round($diff / 60);
			if ($mins <= 1) {
				$mins = 1;
			} /* translators: min=minute */
			$since = sprintf(_n('%s phút', '%s phút', $mins), $mins);
		} else if (($diff <= 86400) && ($diff > 3600)) {
			$hours = round($diff / 3600);
			if ($hours <= 1) {
				$hours = 1;
			}
			$since = sprintf(_n('%s giờ', '%s giờ', $hours), $hours);
		} else if ($diff <= 604800){
			$days = round($diff / 86400);
			if ($days <= 1) {
				$days = 1;
			}
			$since = sprintf(_n('%s ngày', '%s ngày', $days), $days);
		} else if ($diff <= 2592000) {
			$week = round($diff / 604800);
			if ($week <= 1) {
				$week = 1;
			}
			$since = sprintf(_n('%s tuần', '%s tuần', $week), $week);
		} else if ($diff <= 31536000) {
			$month = round($diff / 2592000);
			if ($month <= 1) {
				$month = 1;
			}
			$since = sprintf(_n('%s tháng', '%s tháng', $month), $month);
		} else if ($diff >= 31536000) {
			$years = round($diff / 31536000);
			if ($years <= 1) {
				$years = 1;
			}
			$since = sprintf(_n('%s năm', '%s năm', $years), $years);
		}
		return $since;
	}

// Creating the widget 
class dc_mpu_code_widget extends WP_Widget {

function __construct() {
parent::__construct(
	// Base ID of your widget
	'dc_mpu_code_widget', 
	
	// Widget name will appear in UI
	__('Dubleclick MPU Widget', 'dc_mpu_code_widget'), 

	// Widget description
	array( 'description' => __( 'Simply outputting the mpu adcode as defined in the settings into sidebar.', 'dc_mpu_code_widget' ), ) 
	);
}

// Creating widget front-end
// This is where the action happens
	public function widget( $args, $instance ) {
	$title = apply_filters( 'hb_doubleclick_mpu', $instance['title'] );
	global $mpu;
	// before and after widget arguments are defined by themes
	echo $args['before_widget'];
	if ( ! empty( $title ) )
		echo $args['before_title'] . $title . $args['after_title'];
	
		// This is where you run the code and display the output
		echo $mpu;
	}
		
	// Widget Backend 
	public function form( $instance ) {
	if ( isset( $instance[ 'title' ] ) ) {
	$title = $instance[ 'title' ];
	} 
	// Widget admin form
	
	echo "<p>This Widget will dispay the lower MPU in the sidebar. Please set the correct doubleclick ocdde under Options -> Advertising or diretly under the Category.</p>";
	
	}
	
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
	
	return $instance;
	}
} // Class wpb_widget ends here

// Register and load the widget
function wpb_load_widget() {
	register_widget( 'dc_mpu_code_widget' );
}
add_action( 'widgets_init', 'wpb_load_widget' );
/*
if ( ! is_admin() ) {
	add_filter('pre_get_posts', 'query_cpost_type');
	function query_cpost_type($query) {
	  if(is_category() || is_tag()) {
	    $post_type = get_query_var('post_type');
		if($post_type)
		    $post_type = $post_type;
		else
		    $post_type = array('post','thuong-hieu');
	    $query->set('post_type',$post_type);
		return $query;
	    }
	}
	
	add_filter('pre_get_posts', 'query_dpost_type');
	function query_dpost_type($query) {
	  if(is_category() || is_tag()) {
	    $post_type = get_query_var('post_type');
		if($post_type)
		    $post_type = $post_type;
		else
		    $post_type = array('post','chan-dung');
	    $query->set('post_type',$post_type);
		return $query;
	    }
	}
	
	add_filter('pre_get_posts', 'query_epost_type');
	function query_epost_type($query) {
	  if(is_category() || is_tag()) {
	    $post_type = get_query_var('post_type');
		if($post_type)
		    $post_type = $post_type;
		else
		    $post_type = array('post','fashion-trends');
	    $query->set('post_type',$post_type);
		return $query;
	    }
	}
}
*/

/***********************************************/
/**** Reducing the post object select field ****/
/***********************************************/
/* Reducing the post object select field to just add the last 100 posts created */
function my_post_object_query( $args, $field, $post )
{
    // modify the order
    $args['posts_per_page'] = '100';
	$args['orderby'] = 'date';
	$args['order'] = 'DESC';
	
    return $args;
}

// filter for every field
add_filter('acf/fields/post_object/query', 'my_post_object_query', 10, 3);


// remove wp version param from any enqueued scripts
function vc_remove_wp_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}

add_filter( 'style_loader_src', 'vc_remove_wp_ver_css_js', 9999 );
add_filter( 'script_loader_src', 'vc_remove_wp_ver_css_js', 9999 );



function clean_header(){
	wp_deregister_script( 'comment-reply' );
}
add_action('init','clean_header');

function link_to_img_server( $html) {
	$imgServer = IMG_SERVER;
    $html = str_replace( get_site_url() .'/wp-content/uploads', $imgServer ."/wp-content/uploads", $html );
    return $html;
}
//link to image server
add_filter( 'post_thumbnail_html', 'link_to_img_server', 10, 1 );

function bz_the_content_filter($content) {
	$imgServer = IMG_SERVER;
	$boServer = "http://bazaarvietnam.vn:3979";
	$foServer = "http://bazaarvietnam.vn";
    $content = str_replace( $boServer .'/wp-content/uploads', $imgServer ."/wp-content/uploads", $content );
	$content = str_replace( get_site_url() .'/wp-content/uploads', $imgServer ."/wp-content/uploads", $content );

    //Remove for articles so old
	$content = str_replace( $foServer .'/wp-content/uploads', $imgServer ."/wp-content/uploads", $content );
	
	//Remove for link only
	$content = str_replace( $boServer , $foServer , $content );
	
  return $content;
}

add_filter( 'the_content', 'bz_the_content_filter' );



function getImgServerUrl ( $url ) {
	$imgServer = IMG_SERVER;
	$boServer = "http://bazaarvietnam.vn:3979";
	$url = str_replace( $boServer .'/wp-content/uploads', $imgServer ."/wp-content/uploads", $url );
	$url = str_replace( get_site_url() .'/wp-content/uploads', $imgServer ."/wp-content/uploads", $url );    
	return $url;
}

add_filter( 'wpseo_opengraph_image', 'getOGImgServerUrl', 10, 1 );//for og:image
function getOGImgServerUrl ( $url ) {
	global $wp_query;
  	$post_id = get_the_ID();
  	$url = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id), 'slideshow-landscape' )[0];

	$imgServer = IMG_SERVER;
	$boServer = "http://bazaarvietnam.vn:3979";
	$url = str_replace( $boServer .'/wp-content/uploads', $imgServer ."/wp-content/uploads", $url );
	$url = str_replace( get_site_url() .'/wp-content/uploads', $imgServer ."/wp-content/uploads", $url );    
	return $url;
}


add_filter( 'xmlrpc_methods', 'sar_block_xmlrpc_attacks' );
function sar_block_xmlrpc_attacks( $methods ) {
   unset( $methods['pingback.ping'] );
   unset( $methods['pingback.extensions.getPingbacks'] );
   return $methods;
}

add_filter( 'wp_headers', 'sar_remove_x_pingback_header' );
function sar_remove_x_pingback_header( $headers ) {
   unset( $headers['X-Pingback'] );
   return $headers;
}

// Color post status backend
add_action('admin_head', 'post_color_css');
function post_color_css() {
    //Change the #ccc to what hex color you want
    print '
        <style type="text/css">
        #the-list *, .widefat * {word-wrap:normal;}

        #the-list .status-publish {background-color: #a7d9ee /*#4D4172*/ !important;}
        #the-list .status-draft {background-color: #ccc}
        #the-list .status-for-ic {background-color: #FFB6C1 !important;}
        #the-list .status-for-me {background-color: #f7ea7d !important;}
        #the-list .status-for-editor {background-color: #BADFB3 !important;}
        #the-list .status-back-to-editor {background-color: #F8C0C0 !important;}
        #the-list .status-back-to-journalist {background-color: #FCBA7B !important;}
        </style>
        ';
}

/**
 * Remove the 'description' column from the table in 'edit-tags.php'
 * but only for the 'post_tag' taxonomy
 */
add_filter('manage_edit-post_tag_columns', function ( $columns ) 
{
    if( isset( $columns['description'] ) )
        unset( $columns['description'] );   

    return $columns;
} );

function yst_wpseo_change_og_locale( $locale ) {
return 'vi_VN';
}
add_filter( 'wpseo_locale', 'yst_wpseo_change_og_locale' );


 
?>