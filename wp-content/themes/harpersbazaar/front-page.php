<?php get_header(); ?>

<div id="content" role="main">

	<?php 
		if (get_field('promo_slide', 'option')) {
			get_template_part("parts/homepagepromo");
		}
	?>

	<section class="latest-articles">

		<div class="container clearfix">

			<div class="row">
				
				<h3 class="barred-heading"><span><?php _e("Mới nhất trên Harper's Bazaar", 'harpersbazaar'); ?></span></h3>

				<?php 
				$runway_cat = get_category_by_slug( 'runway' );
				$excluded_cat_id = $runway_cat->term_id;
				$the_query = new WP_Query(
					array (
						'post_type' => array ('post', 'fashion-trends'),
						'showposts' => '12',
						'offset'	=> 3,
						'category__not_in' => array( $excluded_cat_id )
					)
				);
			
				if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post(); 
				?>

				<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">

					<a href="<?php the_permalink() ?>"><?php the_post_thumbnail('latest-article-thumb'); ?></a>

					<header class="article-header">

						<h2 class="h2"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
						<div class="article-meta">
							<p class="byline vcard">
								<?php if (get_post_type()!='post'){ ?>
									<?php 
										$post_type_loc = get_post_type();
										$post_type_loc = get_post_type_object($post_type_loc);
									?>
									<?php printf(__('trong %1s.', 'harpersbazaar'), $post_type_loc->labels->name ); ?>
								<?php }else { ?>
									<?php printf(__('trong %1$s.', 'harpersbazaar'), get_the_category_list(', ')); ?>
								<?php } ?>
							</p>
						</div>

					</header> <!-- end article header -->

				</article> <!-- end article -->

				<?php endwhile; ?>

				<?php else : ?>

					<div id="post-not-found" class="hentry clearfix">
						<p><?php _e("No articles in this category.", "harpersbazaar"); ?></p>
					</div>

				<?php endif; ?>
			</div>
				
		</div> <!-- end .container -->

	</section> <!-- end .latest-articles -->

	<section class="featured">
		<div class="container">
			
			<?php //wp_reset_query();
				$args = array(
					'post_status' => 'publish',
					'post_type' => 'post',
					'order'	=> 'DESC',
					'orderby'	=> 'date',
					'ignore_sticky_posts' => true,
					'posts_per_page' => 1,
					'showposts' => 1,
					'tax_query' => array(
				        array(                
				            'taxonomy' => 'post_format',
				            'field' => 'slug',
				            'terms' => array(
				                'post-format-gallery'
				            ),
				            // 'operator' => 'NOT IN'
				        )
			       	)
				);
				$loop = new WP_Query( $args );
				if ( $loop->have_posts() ) {
					while ($loop->have_posts()) : $loop->the_post(); 
						get_template_part("parts/homepage-featured-article-auto"); 
					endwhile;
				}else{
					get_template_part("parts/homepage-featured-article"); 
				}
				wp_reset_query(); ?>

			<div class="newsletter-form newsletter-form-home container">
				<div class="row">
					<h3><?php _e("Cập nhật ngay", "harpersbazaar"); ?></h3>
					<p><?php _e("Đăng ký nhận email mỗi tuần và cập nhật xu hướng mới nhất", "harpersbazaar"); ?></p>
					<?php gravity_form("Newsletter", $display_title=false, $display_description=false, $display_inactive=false, $field_values=null, $ajax=true); ?>
				</div>
			</div>
		</div>
	</section>

</div> <!-- end #content -->

<?php get_footer(); ?>